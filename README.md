## Lost In Translation

Link to [demo](https://damp-plateau-65806.herokuapp.com/)

Assignment [doc](./assignment.pdf)


### Built With

- ReactJS

Dependencies are listed in [package.json](./package.json)

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

[VSCode](https://code.visualstudio.com/) or any other editor.

[NodeJS](https://nodejs.org/en/)

### Installation

1. Clone the repo
   ```sh
   git clone git@gitlab.com:druwan/lost-in-translation.git
   ```
2. Enter the cloned project folder
   ```sh
   cd lost-in-translation
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. Run the app
   ```js
   npm start
   ```

## Usage

How to use the application

1. Fill in a username and press "Sign in"
2. In the navbar on the top of the page, you can navigate to the different pages of the application.
3. In the translate page, enter text you wish to be translated.
4. Press the translate button and you will get a series of letters in sign-language form.
5. You can enter your own profile to see your 10 latest translations you have entered.
6. If you wish, you can delete the records of the 10 latest words and see if there is more words you have translated.
7. You log out of the application by hitting the "logout".


<!-- CONTACT -->
## Contributors & Contact

[Nils Jacobsen](https://gitlab.com/nils_jacobsen)

[Christopher Vestman](https://github.com/druwan)

