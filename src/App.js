import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "./views/Login";
import Profile from "./views/Profile";
import Translate from "./views/Translate";
import Navbar from "./components/Navbar/Navbar";

function App() {
  return (
    <Router>
      <header>
        <Navbar />
      </header>
      <Routes>
        <Route path="/" exact element={ <Login /> } />
        <Route path="/translations" element={ <Translate /> } />
        <Route path="/profile" element={ <Profile />} />
      </Routes>
    </Router>
  );
}

export default App;
