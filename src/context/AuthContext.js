import React, { createContext, useState } from "react";

export const AuthContext = createContext();
function AuthContextProvider(props) {
  const [isAuthenticated, setIsAuthenticated] = useState(true);
  return (
    <AuthContext value={[isAuthenticated, setIsAuthenticated]}>
      {props.children}
    </AuthContext>
  );
}

export default AuthContextProvider;
