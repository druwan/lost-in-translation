import TranslateHeader from "../components/Translate/TranslateHeader"
import withAuth from "../hoc/withAuth"
import { useUser } from "../context/UserContext"
import TranslateActions from "../components/Translate/TranslateActions"



const Translate = () => {

  const {user} = useUser()

  return (
    <>
    <TranslateHeader username={user.username}/>
    <TranslateActions userID={user.id}/>
    </>
  )
}

export default withAuth(Translate)
