import { createHeaders } from './index'

const apiUrl = process.env.REACT_APP_API_URL

const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if(!response.ok) {
            throw new Error('Could not complete request.')
        }
        const data = await response.json()
        return [ null, data ]
    } catch (error) {
        return [ error.message, [] ]
    }
}

const createUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if(!response.ok) {
            throw new Error('Could not create user with username ' + username)
        }

        const data = await response.json()
        return [ null, data ]
    } catch (error) {
        return [ error.message, [] ]
    }
}
    
export const loginUser = async (username) => {
    const [ checkError, user ] = await checkForUser(username)
    
    if(checkError !== null) {
        return [ checkError, null ]
    }
    
    if(user.length > 0) {
        return [ null, user.pop() ]
    }

    return await createUser(username)
}


export const addTranslationHistory = async (userID, translationArray) => {
    try {
         const response = await fetch(`${apiUrl}/${userID}`, {
             method: "PATCH",
             headers: createHeaders(),
             body: JSON.stringify({
                 translations: translationArray
             })
         })
         if (!response.ok) {
             throw new Error("Could not update translations history.")
         }
         return response.json()
    } catch (error) {
        return [ error.message, [] ]
    }
}


export const deleteTranslationHistory = async (userID, translationArray) => {
    try {
        const response = await fetch(`${apiUrl}/${userID}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: translationArray
            })
        })
        if (!response.ok) {
            throw new Error("Could not change delete status.")
        }
        return response.json()
    } catch (error) {
        return [ error.message, []]
    }
}