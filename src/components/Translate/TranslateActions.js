import { useState } from "react"
import { useUser } from "../../context/UserContext"
import { addTranslationHistory } from "../../api/user"; 


const TranslateActions = () => {

    const [submitted, setSubmitted] = useState('');
    const { user } = useUser()
    const [translationCopy, setTranslationCopy] = useState("")

    const signs = importAll(require.context('/src/assets/individual-signs', false, /\.(png|jpe?g|svg)$/));

    function importAll(r) {
        let signs = {};
        r.keys().map((item, index) => { signs[item.replace('./', '')] = r(item); });
        return signs;
    }

    const [translationString, setTranslationString] = useState('');
    
    let brokenPhrase = translationString.toLowerCase().split("")
    
    brokenPhrase = brokenPhrase.map((p, i) => 
    <img src={signs[`${p}.png`]} alt={i} key={"img" + i} style={{height: "60px", width: "60px"}}/>)

    
    const readInput = (event) => {
        if(event.target.value.match("^[a-zA-Z]*$") != null){
        setTranslationString(event.target.value.toLowerCase())
        }
        if(event.target.value === ""){
            setSubmitted(false)
        }
    }

    const handleSubmit = (event) => {
        setTranslationCopy(brokenPhrase)
        event.preventDefault()
        setSubmitted(true)
        
        // API Call
        // READ and fill existing
        const existingTranslations = user.translations
        const newTranslationString = {translation: translationString, deleted: false}
        existingTranslations.unshift(newTranslationString)
        addTranslationHistory(user.id, existingTranslations)
        
        setTranslationString("")
    }

    return(
        <>
        <form onSubmit={handleSubmit}>
            <input value={translationString} name="translationString" onChange={ readInput } id="translationInput"/>
            <button type="submit"><img src={signs['y.png']} alt="signs" style={{width: "50px", height: "30px"}}/></button>
        </form>
            <div style={{width: "800px", height: "400px", border: "solid"}}>
            <p>{translationString}</p>

            {submitted &&
            <p>{translationCopy}</p>
            }
            </div>
        </>
    )

}

export default TranslateActions