const ProfileTranslationHistoryItem = ({ translation }) => {

    return (
        <li>
            { translation.translation }
        </li>
    )
}

export default ProfileTranslationHistoryItem