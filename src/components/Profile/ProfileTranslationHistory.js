import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"
import { deleteTranslationHistory } from "../../api/user";
import { useUser } from "../../context/UserContext";

const ProfileTranslationsHistory = ({ translations }) => {

    const { user,setUser } = useUser()


    const translationsList = user.translations.filter(
            (translation) => !translation.deleted
        ).map(
            (translation, index) => <ProfileTranslationHistoryItem key={ index + "-" + translation } translation={ translation }/>
        ).slice(0,10)

    const handleClearHistoryClick = () => {
        const existingTranslations = [...user.translations]
        // Filter deleted translations 
        // Run 10 times
        let count = 0;
        let index = 0; 
        while(count < 10 && index < existingTranslations.length){
            const translation = existingTranslations[index];
            if(!translation.deleted){
                translation.deleted = true;
                count++;
            }
            index++;
        }
    
        
        deleteTranslationHistory(user.id, existingTranslations).then(()=>{
            setUser((prevUser) => {return {...prevUser, translations:existingTranslations}})
        } )

    }
    return (
        <section>
            <h4>Your translation history</h4>
            <ol>{ translationsList }</ol>
            <button onClick={ handleClearHistoryClick }>Clear History</button>
        </section>
    )

}
export default ProfileTranslationsHistory