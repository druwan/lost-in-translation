import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import { loginUser } from "../../api/user"
import { storageSave } from "../../utils/storage"
import { useNavigate } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"


const usernameConfig = {
    required: true,
    minLength: 3,
}

const LoginForm = () => {
    // Hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    
    
    // Local State
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError ] = useState(null)
    const navigate = useNavigate()
    

    // Side Effects
    useEffect(() => {
        if (user !== null) {
            navigate("translations")
        }
    }, [ user, navigate ]) // Empty dep - only run once


    // Event handlers
    // Navigate to next page after successful user creation
    const onFormSubmit = async ({ username }) => {
        setLoading(true)
        const [ error, userResponse ] = await loginUser(username)
        setLoading(false)

        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            // Will show id!!
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
    }


    // Render Functions
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        } else {
            switch (errors.username.type) {
                case "required":
                    return <span>Username is required!</span>
                case "minLength":
                    return <span>Username too short, (min 3)!</span>
                default:
                    return null            
            }   
        }
    })()

    return (
        <>
            <h2>Who are you?</h2>
            <form onSubmit={ handleSubmit(onFormSubmit) }>
                <fieldset>
                    <label htmlFor="username">Username</label>
                    <input 
                        type="text" 
                        placeholder="What's your name?"
                        { ...register("username", usernameConfig) }
                        style={{ width: "75%" }} 
                    />
                    { errorMessage }
                </fieldset>
                <button type="submit" disabled={ loading }>Login</button>
                
                { loading && <p>Loggin in...</p> }
                { apiError && <p>{ apiError }</p>}
            </form>
        </>
    )
}

export default LoginForm
